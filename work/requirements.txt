CacheMan==2.0.8
curio==0.8
CyHunspell==1.2.1
future==0.16.0
psutil==5.4.3
PyYAML==3.12
six==1.11.0
